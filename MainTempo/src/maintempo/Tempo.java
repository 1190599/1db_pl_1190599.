/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package maintempo;

/**
 *
 * @author User
 */
public class Tempo {

    private static final int MIN_OMISSAO = 0;
    private static final int HORA_OMISSAO = 0;
    private static final int SEGUNDO_OMISSAO = 0;

    private int hora, min, segundo;
    private String horario;

    public Tempo(int hora, int min, int segundo) {
        this.hora = hora;
        this.min = min;
        this.segundo = segundo;
    }

    public Tempo(int hora, int min, int segundo, String horario) {
        this.hora = hora;
        this.min = min;
        this.segundo = segundo;
        this.horario = horario;
    }

    public Tempo() {
        hora = HORA_OMISSAO;
        min = MIN_OMISSAO;
        segundo = SEGUNDO_OMISSAO;
    }

    //==========================================================================
    public String toString() {
        if (horario != null) {
            return hora + ":" + min + ":" + segundo + ":" + horario;
        } else {
            return hora + ":" + min + ":" + segundo;
        }

    }

    public void setIncreaseOneSecond() {
      
        if (segundo != 59) {

            segundo++;
        } else {
            if (min != 59) {
                min++;
                segundo = 0;
            } else {
                if (hora != 23) {
                    segundo = 0;
                    min = 0;
                    hora++;
                }else{
                         segundo = 0;
                    min = 0;
                    hora=0  ;
                        }
                
            }

        }
       

    }

    public void isGreaterThan(Tempo tempo2) {
        if (getHora() > tempo2.getHora()) {
            System.out.println("O tempo 1 é maior");
        } else {
            if (getHora() < tempo2.getHora()) {
                System.out.println("O tempo 2 é maior");

            } else {
                if (getMin() > tempo2.getMin()) {
                    System.out.println("O tempo 1 é maior");
                } else {
                    if (getMin() < tempo2.getMin()) {
                        System.out.println("O tempo 2 é maior");

                    } else {
                        if (getSegundo() > tempo2.getSegundo()) {
                            System.out.println("O tempo 1 é maior");

                        } else {
                            if (getSegundo() < tempo2.getSegundo()) {
                                System.out.println("O tempo 2 é maior");

                            } else {
                                System.out.println("O tempos são iguais");
                            }
                        }
                    }
                }

            }

        }

    }

    public void isGreaterThan(int hora, int min, int segundo) {
        this.hora = hora;
        this.min = min;
        this.segundo = segundo;
        if (getHora() > hora) {
            System.out.println("O tempo 1 é maior");
        } else {
            if (getHora() < hora) {
                System.out.println("O tempo 2 é maior");

            } else {
                if (getMin() > min) {
                    System.out.println("O tempo 1 é maior");
                } else {
                    if (getMin() < min) {
                        System.out.println("O tempo 2 é maior");

                    } else {
                        if (getSegundo() > segundo) {
                            System.out.println("O tempo 1 é maior");

                        } else {
                            if (getSegundo() < segundo) {
                                System.out.println("O tempo 2 é maior");

                            } else {
                                System.out.println("O tempos são iguais");
                            }
                        }
                    }
                }

            }

        }

    }

    //======================================================================0
    public int getMin() {
        return min;
    }

    public int getSegundo() {
        return segundo;
    }

    public int getHora() {
        return hora;
    }
    public String getHorario(){
    return horario;
}

}
